FROM python:alpine3.16
COPY . ~/app
WORKDIR ~/app
RUN ["pip", "install", "-r", "requirements.txt"]
CMD ["python", "manage.py", "migrate", "blog"]
CMD ["python", "manage.py", "createsuperuser"]
